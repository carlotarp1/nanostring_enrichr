from nanostring_enrichr.run import exec_nanostring_enrichr
import os


folder_path = os.getcwd()+'/'
data_file = 'Run7merged_106genesv2.xls'
colors_file = 'COLORS.xlsx'
results_path = 'results/results_codeset106/'
gene_sets_file = 'gene_sets.tsv'

signatures_selected = 'IFNg_sig,TAM,CYT'  #type them like this, as a sentece divided by commas and with ' at the beggining and the end

exec_nanostring_enrichr(folder_path, 'data/' + data_file,
                        run_name='',  #if empty current date will be set as run name.
                        plot_size=(7,4),  #size of clustering heatmap (width x height)
                        rand_colors_ctypes=False,  #use or not random colors for cancer types
                        nclusters=3, #number of clusters to be identified
                        sample_class = True,
                        # DO NOT CHANGE ANYTHING BELOW
                        results_path=results_path,
                        colors_file = 'data/'+colors_file,
                        gene_sets_file = gene_sets_file,
                        signatures_selected = signatures_selected)