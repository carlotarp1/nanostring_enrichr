Nanostring_enrichr documentation
-
**nanostring_enrichr** allows to perform gene set enrichment analysis on Nanostring data based on similar approaches 
[Hallett et al 2012a](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3441237/) [Hallett et al 2012b](https://www.nature.com/articles/srep00227).

**INPUT**
Excel file formed by three sheets:

* sheet1: raw counts data (Nsolver tabulated output with Probe Name and the expression values across samples only)
* sheet2: normalised counts data (Nsolver tabulated output with Probe Name and the expression values across samples only)
* sheet3: sample annotations (minimal required fields):
    * NSOLVER_ID (RCCfile ID or the ID used in Nsolver if changed -> not advised)
    * SAMPLE_ID (internal sample ID, free text)
    * CANCER_TYPE (sample cancer type ID, free text)
    * (RUN DATE) (optional)

**nanostring_enricher.run** is the core method that executes:
* **QC**: peforms a quality and batch assessment of the samples and genes analysed.
* **enrichr**: computes the enrichment scores of the specified gene sets
* **aggregation**: performs a sample aggregation in four groups based on the enrichment scores
 
**nanostring_enricher.execute** is the script that the user needs to execute. 

To install it locally run:

`pip install -e path/nanostring_enrichr 
`
