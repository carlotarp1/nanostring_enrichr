import setuptools

setuptools.setup(
    name="nanostring_enrichr",
    version="0.1.0",
    author="Gene expression and cancer lab",
    author_email="jseoanelab@gmail.com",
    description="Code to make gene set enrichment on Nanostring normalized data",
    url="https://",
    packages=['nanostring_enrichr'],
    install_requires=['pandas','seaborn','scipy','scikit-learn','xlrd'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU AFFERO GENERAL PUBLIC LICENSE",
        "Operating System :: OS Independent",
    ],
)
