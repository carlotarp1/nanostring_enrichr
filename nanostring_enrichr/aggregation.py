import pandas as pd
from sklearn.preprocessing import minmax_scale
import operator
import seaborn as sns
from sklearn.cluster import AgglomerativeClustering
from nanostring_enrichr.core_f import *

sns.set(style="white")

def cancer_type_heatmap(sample_clustNAME,sample_annotations,results_path,cluster_order):
    heatmap_l = []
    heatmap_l1 = []
    cancer_type_l = [ c.split(':')[0]  for c in list(sample_annotations.CANCER_TYPE.unique())]
    cancertype_patient = {cancer_type.split(':')[0]:cancer_typedf.SAMPLE_ID.tolist() for cancer_type,cancer_typedf in sample_annotations.groupby('CANCER_TYPE')}
    prop_benefitial = {cancer_type: len([s for s in cancertype_patient[cancer_type] if sample_clustNAME[s]=='1']) for cancer_type in cancer_type_l}
    cancer_type_lsorted = [v[0] for v in sorted(prop_benefitial.items(), key=operator.itemgetter(1))]
    prop_benefitial['PANCANCER'] = len([s for s,clust in sample_clustNAME.items() if clust=='high2'])
    for cluster in cluster_order:
        cancer_types_c = {}
        cancer_types_c1 = {}
        for cancer_type in cancer_type_lsorted:
            samples = cancertype_patient[cancer_type]
            cancer_types_c[cancer_type] = len([s for s in samples if sample_clustNAME[s]==cluster]) / float(len(samples))
            cancer_types_c1[cancer_type] = len([s for s in samples if sample_clustNAME[s]==cluster])
        cancer_types_c['PANCANCER'] = len([s for s,clust in sample_clustNAME.items() if clust ==cluster]) / float(len(sample_clustNAME))
        cancer_types_c1['PANCANCER'] = len([s for s,clust in sample_clustNAME.items() if clust ==cluster])
        heatmap_l.append(cancer_types_c)
        heatmap_l1.append(cancer_types_c1)
    heatmapdf = pd.DataFrame(heatmap_l)
    heatmapdf1 = pd.DataFrame(heatmap_l1)
    cancer_type_lsorted.append('PANCANCER')
    fig = plt.figure(figsize=(3, 2))
    config_rcparams(8)
    sns.heatmap(heatmapdf[cancer_type_lsorted], cmap="Blues", vmax=1, vmin=0, cbar=False,
                yticklabels=cluster_order,xticklabels=cancer_type_lsorted)
    y = 0
    for row in heatmapdf1[cancer_type_lsorted].to_dict(orient='records'):  # go over each gene
        x = 0
        for col in heatmapdf1[cancer_type_lsorted].columns.tolist():  # go over each column
            value = row[col]  # get the sample count
            fontclr="black"
            if heatmapdf[col][y] >= 0.6:
                fontclr = "white"
            if value != 0:
                plt.text(x + 0.5, y + 0.5, str(np.round(value, 2)), verticalalignment='center',
                           horizontalalignment='center', fontsize=7, color=fontclr)
            x += 1
        y += 1
    plt.tight_layout()

    plt.yticks(rotation=0)
    plt.savefig(results_path + 'cancertype_cluster_distribution.svg', dpi=300)
    print('>GENERATED:'+results_path + 'cancertype_cluster_distribution.png')

    fig, ax = plt.subplots(figsize=(3, 2))
    r = range(0,len(heatmapdf.columns))
    barWidth = 1
    sortedheatmapdf = heatmapdf[cancer_type_lsorted]
    colors = ['#241628','#3e5095','#38aaac','#94dcb5','grey']
    for i in range(0,len(sortedheatmapdf)):
        if i == 0:
            cumvec = [0 for v in range(0,len(sortedheatmapdf.columns))]
        dat = sortedheatmapdf.values[i]
        plt.bar(r, dat,bottom=cumvec, color=colors[i], edgecolor='white', width=barWidth)
        cumvec += sortedheatmapdf.values[i]
    plt.ylabel('Proportion of samples')
    plt.legend(dict(zip(range(0,len(cluster_order)),colors)))
    plt.tight_layout()
    plt.savefig(results_path + 'cancertype_cluster_distribution_barplot.svg', dpi=300)
    print('>GENERATED:'+results_path + 'cancertype_cluster_distribution_barplot.png')

    return

def Hclustering(enrichment_scores, colors_ctypes, patient_cancertype, results_path,
                sample_annotations, cancer_types_in_plot, clusterfigsize, plot_color_cancertype,
                nclusters,sampleclassbool, colors_file):

    # Rescale enrichment values and clusterize
    config_rcparams(6)
    enrichment_scores_rescaled = minmax_scale(enrichment_scores, axis=1)
    if cancer_types_in_plot:
        xticks = [s for s in enrichment_scores.columns.tolist()]
    else:
        xticks = enrichment_scores.columns.tolist()
    g = sns.clustermap(enrichment_scores_rescaled, yticklabels=enrichment_scores.index.tolist(),
                       cmap="mako_r",vmin=0,vmax=1,
                       xticklabels=xticks,
                       method='ward',figsize=clusterfigsize)
    samples_ordered = []
    for t in g.ax_heatmap.get_xticklabels():
        tst = t.get_text().split(':')[0]
        samples_ordered.append(tst)
        if plot_color_cancertype:
            t.set_color(colors_ctypes[patient_cancertype[tst]])
    plt.setp(g.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    plt.tight_layout()
    plt.savefig(results_path+'signatures_Hclustering.svg',dpi=300)
    print('>GENERATED:'+results_path + 'signatures_Hclustering.png')

    fig, ax = plt.subplots(figsize=(4, 8))
    config_rcparams(6)
    markers = [plt.Line2D([0, 0], [0, 0], color=color, marker='o', linestyle='') for color in colors_ctypes.values()]
    plt.legend(markers, colors_ctypes.keys(), numpoints=1,loc='upper left')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.tight_layout()
    plt.savefig(results_path + 'cancertype_colorlegend.svg', dpi=300)
    print('>GENERATED:'+results_path + 'cancertype_colorlegend.png')


    #Identify clusters and retrieve sample classification
    enrichment_scores_rescaledT = pd.DataFrame(enrichment_scores_rescaled,columns=enrichment_scores.columns,index=enrichment_scores.index).transpose()
    ward = AgglomerativeClustering(n_clusters=nclusters, linkage='ward').fit(enrichment_scores_rescaledT)
    sample_clust = {s: ward.labels_[i]
                    for i, s in enumerate(enrichment_scores_rescaledT.index.tolist())}
    clust_vals = {}
    for row in enrichment_scores_rescaledT.reset_index().to_dict(orient='records'):
        clust = sample_clust[row['index']]
        if clust not in clust_vals.keys():
            clust_vals[clust] = []
        clust_vals[clust].append(np.mean(list([row[c] for c in row.keys() if c !='index'])))
    clust_vals_m = {c:np.mean(v) for c,v in clust_vals.items()}
    clusters_sorted = [v[0] for v in sorted(clust_vals_m.items(), key=operator.itemgetter(1))]
    order = [str(r) for r in range(0,nclusters)]
    clusters_name = {c:order[i] for i,c in enumerate(clusters_sorted)}
    sample_clustNAME = {s:clusters_name[c] for s,c in sample_clust.items()}


    # Plot heatmap annotations
    if sampleclassbool:
        colors_sc = pd.read_excel(colors_file, sheet_name='sample_class')
        colors_sampleclass = {row['SAMPLE_CLASS']: row['COLOR'] for row in colors_sc.to_dict(orient='records')}
        sample_classd =  {row['SAMPLE_ID']: row['SAMPLE_CLASS'] for row in sample_annotations.to_dict(orient='records')}

    new_cmap = rand_cmap(len(order), type='bright', first_color_black=True, last_color_black=False, verbose=False)
    cluster_colors = {c: new_cmap[i] for i, c in enumerate(order)}

    fig = plt.figure(figsize=clusterfigsize)
    for i in range(0, len(samples_ordered)):
        if samples_ordered[i] in patient_cancertype:
            plt.plot(i, 1, color=colors_ctypes[patient_cancertype[samples_ordered[i]]], marker='s')
            plt.plot(i, 4, color=cluster_colors[sample_clustNAME[samples_ordered[i]]], marker='s')
            if sampleclassbool:
                if sample_classd[samples_ordered[i]] != 'NA':
                    plt.scatter(i, 3, color=colors_sampleclass[sample_classd[samples_ordered[i]]], marker='d')

    plt.tight_layout()
    plt.savefig(results_path + 'signatures_Hclustering_annotations.svg', dpi=300)
    print('>GENERATED:' + results_path + 'signatures_Hclustering_annotations.png')

    #Plot cancer type distribution
    cancer_type_heatmap(sample_clustNAME, sample_annotations, results_path ,order)

    return sample_clustNAME,pd.DataFrame(enrichment_scores_rescaled,columns=enrichment_scores.columns,index=enrichment_scores.index)


def Hclustering_table(sample_cluster,sample_annotations,enrichment_scores_rescaled,results_path):
    enrichment_scores_rescaledT = enrichment_scores_rescaled.transpose().reset_index()

    sample_annotations_enrichment_score = pd.merge(sample_annotations,enrichment_scores_rescaledT,left_on='SAMPLE_ID',right_on='index')
    sample_annotations_enrichment_score['CLUSTER'] = sample_annotations_enrichment_score['SAMPLE_ID'].apply(lambda x:sample_cluster[x])
    valid_cols = [c for c in sample_annotations_enrichment_score.columns.tolist() if c != 'index']

    sample_annotations_enrichment_score[valid_cols].to_csv(results_path+'Hclustering_enrichment_scores.tsv',sep='\t',index=False)
    print('>GENERATED:'+results_path+'Hclustering_enrichment_scores.tsv')
    return


