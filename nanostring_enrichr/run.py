from nanostring_enrichr.QC import runQCanalysis
from nanostring_enrichr.enrich import compute_enrichment
from nanostring_enrichr.aggregation import Hclustering,Hclustering_table
import os,datetime,errno
import pandas as pd

def exec_nanostring_enrichr(folder_path,data_file,run_name, plot_size,TCGA,
                            results_path,rand_colors_ctypes,colors_file,gene_sets_file,nclusters,signatures_selected,sample_class):

    plot_color_cancertype=False
    plot_add_Xcancertype = True

    colors = pd.read_excel(colors_file, sheet_name='cancer_type')
    colors_ctypes = {row['CANCER_TYPE']:row['COLOR'] for row in colors.to_dict(orient='records')}
    print('START')
    if run_name == '':
        now = datetime.datetime.now()
        run_name = 'results_'+str(now.year)+str(now.month)+str(now.day)+'/'
    else:
        run_name = 'results_'+run_name+'/'

    try:
        os.makedirs(results_path+run_name)
    except OSError as e:
        print(results_path+run_name,'already exists, files will be overwritten')
        if e.errno != errno.EEXIST:
            raise

    results_path += run_name
    
    # Load data, run QC and batch normalization control
    colors_ctypes, patient_cancertype, norm_counts_log, genes_annotations, sample_annotations = \
        runQCanalysis(folder_path, data_file, gene_sets_file, results_path, rand_colors_ctypes, colors_ctypes, plot_size, TCGA)

    # Compute enrichment scores
    enrichment_scores = compute_enrichment(norm_counts_log, genes_annotations, signatures_selected)
    norm_enrich = pd.merge(norm_counts_log.set_index('GENE').T.reset_index(),enrichment_scores.T.reset_index(),on='index')
    norm_enrich.to_csv(results_path+'NormCounts_EnrichmentScores.tsv',sep='\t',index=False)

    # Sample aggregation clustering
    #enrichment_scores = pd.read_csv(folder_path+'data/TCGA_signatures.tsv',sep='\t',header=0)
    #enrichment_scores = enrichment_scores.set_index('SIGNATURE')

    sample_cluster, enrichment_scores_rescaled = Hclustering(enrichment_scores,colors_ctypes,patient_cancertype,
                                                             results_path,sample_annotations,plot_add_Xcancertype,plot_size,plot_color_cancertype,
                                                             nclusters,sample_class,colors_file)
    Hclustering_table(sample_cluster, sample_annotations, enrichment_scores_rescaled,results_path)

    print('END')
    return