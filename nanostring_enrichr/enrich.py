import pandas as pd
from scipy.stats import zscore
import seaborn as sns
sns.set(style="white")
import numpy as np
import pkg_resources

def compute_enrichment(norm_counts_log, genes_annotations,signatures_selected):
    print('> STARTING ENRICHMENT')

    gene_signatures = {}
    for annot in signatures_selected.split(','):
        if annot in genes_annotations['ANNOT'].unique().tolist():
            gene_signatures[annot] = genes_annotations[genes_annotations['ANNOT']==annot].GENE.tolist()
        else:
            print('\t>> WARNING: the following signature name is not found in the gene sets file:', annot)

    # Generate Z-score matrix
    genes_z = []
    patients = [c for c in norm_counts_log.columns.tolist() if c != 'GENE']
    for row in norm_counts_log.to_dict(orient='records'):
        valsZ = zscore([row[c] for c in patients])
        values_d = {c: valsZ[i] for i, c in enumerate(patients)}
        values_d['GENE'] = row['GENE']
        genes_z.append(values_d)
    norm_counts_logZ = pd.DataFrame(genes_z)

    # Compute enrichment score
    signature_enrich = []

    for signature, genes_l in gene_signatures.items():
        genesfound = list(set(norm_counts_log['GENE'].tolist()).intersection(set(genes_l)))
        if len(genesfound) != len(genes_l):
            print('\t>> WARNING:',signature,len(genesfound),'/',len(set(genes_l)),'genes found, less genes are considered, see list')
            print('\t',genesfound)
        else:
            print('\t<< GOODtoRUN:',signature)
        if len(genesfound) != 0 and len(genesfound)/float(len(set(genes_l))) > 0.5:
            counts_signature = norm_counts_logZ[norm_counts_logZ['GENE'].isin(genesfound)]
            vals_d ={c: counts_signature[c].mean() for i, c in enumerate(patients)}
            vals_d ['SIGNATURE'] = signature
            signature_enrich.append(vals_d)
        else:
            print('\t>> WARNING:', signature, ' DISCARDED')

    dfsignature_enrich = pd.DataFrame(signature_enrich)
    # values_d = {}
    # for col in dfsignature_enrich.columns.tolist():
    #     if col == 'SIGNATURE':
    #         values_d[col] = 'LT'
    #     else:
    #         values_d[col] = np.mean(dfsignature_enrich[col])
    # dfsignature_enrich = dfsignature_enrich.append(pd.DataFrame([values_d]))
    dfsignature_enrich = dfsignature_enrich.set_index('SIGNATURE')
    return dfsignature_enrich