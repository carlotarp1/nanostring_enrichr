import matplotlib.pyplot as plt
import numpy as np

def config_rcparams(commonFontsize):
	plt.rcParams['font.family'] = ['sans-serif']
	plt.rcParams['font.sans-serif'] = ['arial']
	plt.rcParams['font.size'] = commonFontsize
	plt.rcParams['axes.labelsize'] = commonFontsize
	plt.rcParams['xtick.labelsize'] = commonFontsize
	plt.rcParams['ytick.labelsize'] = commonFontsize
	plt.rcParams['axes.titlesize'] = commonFontsize
	plt.rcParams['svg.fonttype'] = 'none'
	plt.rcParams['mathtext.fontset'] = 'custom'
	plt.rcParams['mathtext.cal'] = 'arial'
	plt.rcParams['mathtext.rm'] = 'arial'
	plt.rcParams['axes.edgecolor'] = 'black'
	plt.rcParams['ytick.color'] = 'black'
	plt.rcParams['axes.linewidth'] = 0.7
	plt.rcParams['xtick.major.width'] = 0.7
	plt.rcParams['ytick.major.width'] = 0.7
	plt.rcParams['xtick.major.size' ] = 3
	plt.rcParams['ytick.major.size' ] = 3

def apply_log(v):
    v = float(v) + 1
    if float(v) <= 0:
        val = -6
    else:
        val = np.log2(float(v))
    return val

def rand_cmap(nlabels, type='bright', first_color_black=True, last_color_black=False, verbose=True):
    """
    Creates a random colormap to be used together with matplotlib. Useful for segmentation tasks
    :param nlabels: Number of labels (size of colormap)
    :param type: 'bright' for strong colors, 'soft' for pastel colors
    :param first_color_black: Option to use first color as black, True or False
    :param last_color_black: Option to use last color as black, True or False
    :param verbose: Prints the number of labels and shows the colormap. True or False
    :return: colormap for matplotlib
    """
    from matplotlib.colors import LinearSegmentedColormap
    import colorsys
    import numpy as np


    if type not in ('bright', 'soft'):
        print ('Please choose "bright" or "soft" for type')
        return

    if verbose:
        print('Number of labels: ' + str(nlabels))

    # Generate color map for bright colors, based on hsv
    if type == 'bright':
        randHSVcolors = [(np.random.uniform(low=0.0, high=1),
                          np.random.uniform(low=0.2, high=1),
                          np.random.uniform(low=0.9, high=1)) for i in range(nlabels)]

        # Convert HSV list to RGB
        randRGBcolors = []
        for HSVcolor in randHSVcolors:
            randRGBcolors.append(colorsys.hsv_to_rgb(HSVcolor[0], HSVcolor[1], HSVcolor[2]))

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]


    # Generate soft pastel colors, by limiting the RGB spectrum
    if type == 'soft':
        low = 0.6
        high = 0.95
        randRGBcolors = [(np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high)) for i in range(nlabels)]

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]


    return randRGBcolors

def rescale(x):
    vals_r = []
    for val in x:
        vals_r.append( (val - min(x)) / (max(x) - min(x)) )
    return vals_r


def readGmtFile(gmt_file):
    gene_dict = {}  # key: gene, value
    for line in open(gmt_file, 'r'):
        s = line.split('\t')
        gene_set = s[0]
        genes = s[2:]
        gene_dict[gene_set] = [g for g in genes if g != '' and g !='\n']
    return gene_dict