import pandas as pd
import seaborn as sns
sns.set(style="white")
from scipy.stats.mstats import gmean
from nanostring_enrichr.core_f import *
from scipy.cluster.hierarchy import dendrogram, linkage
import pkg_resources
import operator
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
config_rcparams(7)



def runQCanalysis(folder_path,data_file ,gene_sets_file,results_path, rand_colors_ctypes, colors_ctypes, plot_size, TCGA):
    #### LOAD DATA ###################################################################################
    if TCGA == False:
        xls = pd.ExcelFile(folder_path+data_file)
        raw_counts = pd.read_excel(folder_path+data_file, sheet_name=xls.sheet_names[0])
        raw_counts = raw_counts.dropna()
        norm_counts = pd.read_excel(folder_path+data_file, sheet_name=xls.sheet_names[1])
        norm_counts = norm_counts.dropna()
        sample_annot = pd.read_excel(folder_path+data_file, sheet_name=xls.sheet_names[2],header=0)
        sample_annot = sample_annot.fillna('NA')
    else:
        sample_annot = pd.read_csv(folder_path+'data/TCGA_SAMPLES_SELECTION.tsv',sep='\t',header=0)
        norm_counts = pd.read_csv(folder_path+'data/TCGA_PANCANCER_immuno.tsv',sep='\t',header=0)
        raw_counts = pd.read_csv(folder_path+'data/TCGA_PANCANCER_immuno.tsv',sep='\t',header=0)

    if 'RUN_DATE' not in sample_annot.columns.tolist():
        sample_annot['RUN_DATE'] = sample_annot['NSOLVER_ID'].apply(lambda x:str(x.split('_')[0]))
    else:
        sample_annot['RUN_DATE'] = sample_annot['RUN_DATE'].apply(lambda x:str(x))

    print('Sample annot n=',len(set(sample_annot.NSOLVER_ID.tolist())))
    print('RAW sheet columns n=',len(set(raw_counts.columns.tolist())))
    print('NORM sheet columns n=',len(set(norm_counts.columns.tolist())))
    print('Sample IDs common between three sheets n=',len(set(sample_annot.NSOLVER_ID.tolist()).intersection(set(norm_counts.columns.tolist()))))
    print('NOT COMMON Sample IDs between three sheets',set(raw_counts.columns.tolist())-set(sample_annot.NSOLVER_ID.tolist()))

    sample_annot = sample_annot[sample_annot['DISCARD']!=True]

    DATA_PATH = pkg_resources.resource_filename('nanostring_enrichr', 'data/')
    genes_annotations = pd.read_csv(DATA_PATH + gene_sets_file, sep='\t', header=0)

    #### PRE-PROCESS ###################################################################################
    # Generate dicts to access sample and gene annot data faster
    patient_run = {row['SAMPLE_ID']: row['RUN_DATE'] for row in sample_annot.to_dict(orient='records')}
    patient_cancertype = {row['SAMPLE_ID']: row['CANCER_TYPE'].split(':')[0] for row in
                          sample_annot.to_dict(orient='records')}
    patient_sampleid = {row['SAMPLE_ID']: row['NSOLVER_ID'] for row in sample_annot.to_dict(orient='records')}
    sampleid_patient = {row['NSOLVER_ID']: row['SAMPLE_ID'] for row in sample_annot.to_dict(orient='records')}
    gene_geneset = {row['GENE']: row['ANNOT'] for row in genes_annotations.to_dict(orient='records')}

    valid_cols = ['GENE']
    valid_cols.extend(list(set(sample_annot.NSOLVER_ID.tolist()).intersection(set(norm_counts.columns.tolist())).intersection(set(raw_counts.columns.tolist()))))

    if TCGA == False:
        cols = norm_counts.columns.tolist()
        norm_counts = norm_counts.rename(columns={cols[0]:'GENE'})
        raw_counts = raw_counts.rename(columns={cols[0]:'GENE'})
        norm_counts_s = norm_counts[valid_cols]
        raw_counts_s = raw_counts[valid_cols]

        print('>',len(valid_cols)-1,'samples considered taking into account DISCARD column')
        ####################################################################################################

        ### Convert to log2 the expression values
        exp_log = pd.DataFrame()
        for col in raw_counts_s.columns.tolist():
            if col == 'GENE':
                exp_log[col] = raw_counts_s[col]
            else:
                exp_log[col] = raw_counts_s[col].apply(lambda x: apply_log(x))
        exp_log_renamed = exp_log.rename(columns=sampleid_patient)
        exp_log_renamed = exp_log_renamed.set_index('GENE').transpose()

        expnorm_log = pd.DataFrame()
        for col in norm_counts_s.columns.tolist():
            if col == 'GENE':
                expnorm_log[col] = norm_counts_s[col]
            else:
                expnorm_log[col] = norm_counts_s[col].apply(lambda x: apply_log(x))
        expnorm_log_renamed = expnorm_log.rename(columns=sampleid_patient)

        # Generate array with colors
        if rand_colors_ctypes:
            ctypes_ul = list(set(patient_cancertype.values()))
            new_cmap = rand_cmap(len(ctypes_ul), type='bright', first_color_black=True, last_color_black=False, verbose=False)
            colors_ctypes = {c: new_cmap[i] for i, c in enumerate(ctypes_ul)}

        rundate_ul = sample_annot['RUN_DATE'].unique().tolist()
        shapes = [ 'o', 'v', '+', '<', '>', '^', 'd', 'p', '8', 'h', '_', 'X', '.', '4', '*', '<', '>', '^', 'd']
        shapes_rundate = {c: shapes[i] for i, c in enumerate(rundate_ul)}

        fig, ax = plt.subplots(figsize=(4, 8))
        config_rcparams(6)
        markers = [plt.Line2D([0, 0], [0, 0], color='grey', marker=shap, linestyle='') for shap in shapes_rundate.values()]
        plt.legend(markers, shapes_rundate.keys(), numpoints=1,loc='upper left')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        plt.tight_layout()
        plt.savefig(results_path + 'normBATCH_runlegend.png', dpi=300)
        print('> GENERATED:'+results_path + 'normBATCH_runlegend.png')

        ####################################################################################################

        # #### QUALITY CONTROL (raw data) ###################################################################################
        # # Check mean HKs counts per sample
        c = 0
        samples_to_discard = []
        samplesID_to_discard =[]
        raw_counts_hk = raw_counts_s[raw_counts_s['GENE'].isin(genes_annotations[genes_annotations['ANNOT'] == 'HK']['GENE'].tolist())]

        raw_counts_s.rename(columns=sampleid_patient).to_csv(results_path+'raw_counts.tsv',sep= '\t',index=False)
        fout2 = open(results_path + 'HKvalues.tsv', 'w')
        for patient,rccID in patient_sampleid.items():
            valslow = [1 for v in raw_counts_hk[rccID].tolist() if v < 50 ]
            fout2.writelines(patient+'\t'+str(gmean(raw_counts_hk[rccID].tolist()))+'\n')
            if gmean(raw_counts_hk[rccID].tolist()) < 50:
            #if sum(valslow)>=2:
                print('\t>> WARNING:', patient,'geometric mean of HK genes', gmean(raw_counts_hk[rccID].tolist()), 'HKs gmean counts <50')
                print('list of counts per HK',raw_counts_hk[rccID].tolist())
                samples_to_discard.append(rccID+'\t'+patient+'\tmeanHK<50')
                samplesID_to_discard.append(patient)
                c+=1
        fout2.close()
        if c != 0:
            print('>>',c,'samples WILL be discarded (HK counts <50)')
            # print('>> NORMALIZATION should be run again excluding them.')
            fout = open(results_path+'samples_to_discard.txt','w')
            fout.writelines('NSOLVER_ID\tSAMPLE_ID\n')
            for p in samples_to_discard:
                fout.writelines(p+'\n')
            fout.close()
            print('> GENERATED:',results_path+'samples_to_discard.txt')

        # Check mean counts per gene across all samples
        ##  for row in raw_counts_s.to_dict(orient='records'):
        ##     if 'POS_' not in row['GENE'] and 'NEG_' not in row['GENE']:
        ##          gmean_gene = gmean([float(row[c]) for c in raw_counts_s.columns.tolist() if c !='GENE'])
        ##          if gmean_gene < 50:
        ##              if row['GENE'] in gene_geneset.keys():
        ##                  print('\t>> WARNING:', row['GENE'],'(',gene_geneset[row['GENE']],'geneset)', 'mean counts =',np.round(gmean_gene,2))
        ##              else:
        ##                  print('\t>> WARNING:', row['GENE'], 'mean counts =',np.round(gmean_gene,2))
        ####################################################################################################

        #### BATCH CONTROL (normalized data) ###################################################################################
        # Dendogram, hierarchical clustering of the patients
        fig, ax = plt.subplots(ncols=2,figsize=(4,9), gridspec_kw={'width_ratios':[10,1]})
        Z = linkage(exp_log_renamed, 'ward')  # Calculate the distance between each sample
        with plt.rc_context({'lines.linewidth': 0.5}):
            dendrogram(Z, labels=exp_log_renamed.index.tolist(), leaf_rotation=0, orientation="left",ax=ax[0])
        for t in ax[0].yaxis.get_ticklabels():
            #print(t.get_text(),patient_cancertype[t.get_text()])
            t.set_color(colors_ctypes[patient_cancertype[t.get_text()]])
        for i, t in enumerate(ax[0].yaxis.get_ticklabels()):
            ax[1].scatter(0, i, color='grey', marker=shapes_rundate[str(patient_run[t.get_text()])], s=20)
        markers = [plt.Line2D([0, 0], [0, 0], color=color, marker='o', linestyle='') for color in colors_ctypes.values()]
        plt.sca(ax[0])
        plt.legend(markers, colors_ctypes.keys(), numpoints=1,loc='upper left')

        for n in [0,1]:
            ax[n].spines['top'].set_visible(False)
            ax[n].spines['right'].set_visible(False)
            ax[n].spines['bottom'].set_visible(False)
            ax[n].spines['left'].set_visible(False)
            ax[n].get_xaxis().set_visible(False)

        ax[1].set_ylim(0,i)
        ax[1].get_yaxis().set_visible(False)

        plt.tight_layout()
        plt.savefig(results_path+'normBATCH_dedogram.png',dpi=300)

        print('> GENERATED:',results_path+'normBATCH_dedogram.png')

        ## PCA analysis
        X_my = exp_log_renamed.values
        X_my_std = StandardScaler().fit_transform(X_my)
        pca = PCA(n_components=2)
        principalComponents = pca.fit_transform(X_my_std)
        PCAdf = pd.DataFrame(data = principalComponents
                     , columns = ['principal component 1', 'principal component 2'])
        PCAdf['CANCER_TYPE'] = [patient_cancertype[samp] for samp in exp_log_renamed.index]
        PCAdf['RUN'] = [patient_run[samp] for samp in exp_log_renamed.index]
        PCAdf['PATIENT'] = [samp for samp in exp_log_renamed.index]

        fig = plt.figure(figsize = (5,5))
        ax = fig.add_subplot(1,1,1)
        ax.set_xlabel('Principal Component 1', fontsize = 15)
        ax.set_ylabel('Principal Component 2', fontsize = 15)
        ax.set_title('2 component PCA', fontsize = 20)
        targets = PCAdf['CANCER_TYPE'].unique().tolist()

        for row in PCAdf.to_dict(orient='records'):
            plt.scatter(row['principal component 1'],row['principal component 2'],
                        marker=shapes_rundate[str(patient_run[row['PATIENT']])], color=colors_ctypes[row['CANCER_TYPE']],
                        s=50)

        ax.legend(loc='center left',bbox_to_anchor=(1.04,0.5))
        ax.grid()
        plt.savefig(results_path+'normBATCH_PCA.png',dpi=300)

        ## HK distribution per sample
        fig = plt.figure(figsize=plot_size)
        config_rcparams(6)
        def trimmer_500(v):
            v = float(v)
            if v >= 100:
                v = 100
            return v
        raw_counts_hkT = raw_counts_hk.set_index('GENE').T
        for hk in raw_counts_hkT.columns.tolist():
            plt.scatter(range(0,len(raw_counts_hkT)),[trimmer_500(v) for v in raw_counts_hkT[hk].tolist()])
        plt.hlines(xmin=0,xmax=len(raw_counts_hkT),y=50)
        plt.xticks(range(0,len(raw_counts_hkT)),[sampleid_patient[p] for p in raw_counts_hkT.index.tolist()],rotation=90)
        plt.savefig(results_path+'normBATCH_HKdotplot.svg',dpi=300)
        plt.clf()
        ####################################################################################################

        ### REMOVE DUPLICATED SAMPLES ###############################################################################################
        sample_annot['SAMPLE_ID_UNIQUE'] = sample_annot['SAMPLE_ID'].apply(lambda x:x.split('_')[0])
        for sampleU,samplesdf in sample_annot.groupby('SAMPLE_ID_UNIQUE'):
            if len(samplesdf) > 1:
                samples = samplesdf.SAMPLE_ID.tolist()
                print('\t>> WARNING:',sampleU,'has',len(samples),'duplicates')
                samples_gmeanHK = {s:gmean(raw_counts_hk[patient_sampleid[s]].tolist()) for s in samples}
                sample_keep = max(samples_gmeanHK.items(), key=operator.itemgetter(1))[0]
                print('list of duplicates',samples)
                for s in samples:
                    if s!= sample_keep:
                        samples_to_discard.append(patient_sampleid[s]+'\t'+s+'\tduplicated')
                        samplesID_to_discard.append(s)
                        c+=1
        i = 0
        samples_gmeanHK = {s: gmean(raw_counts_hk[patient_sampleid[s]].tolist()) for s in sample_annot.SAMPLE_ID}
        fig,ax=plt.subplots(figsize=plot_size)
        for s,v in samples_gmeanHK.items():
            if v < 50:
                color ='red'
            else:
                color='grey'
            plt.bar(i,v,color=color,width=0.5)
            i+=1
        plt.xticks(range(0,i),samples_gmeanHK.keys(),rotation=90)
        plt.tight_layout()
        plt.savefig(results_path+'normBATCH_HKbar.svg',dpi=300)
        plt.clf()

        if c != 0:
            print('>>',c,'samples should be discarded (HK mean counts <50 or duplicated)')
            fout = open(results_path+'samples_to_discard.txt','w')
            fout.writelines('NSOLVER_ID\tSAMPLE_ID\n')
            for p in samples_to_discard:
                fout.writelines(p+'\n')
            fout.close()
            print('> GENERATED:',results_path+'samples_to_discard.txt')
        ####################################################################################################
        # samplesID_to_discard = []
        sample_annot_valid = sample_annot[~sample_annot['SAMPLE_ID'].isin(samplesID_to_discard)]
        #sample_annot_valid = sample_annot_valid[sample_annot_valid['SAMPLE_ID'].isin(norm_counts.columns)]

        samples_forced = sample_annot[sample_annot['FORCE_INCLUSION']==True]['SAMPLE_ID'].tolist()
        if len(samples_forced) > 0:
            print('>',len(samples_forced), 'forced input samples: ',samples_forced)
            sample_annot_valid = sample_annot_valid.append(sample_annot[sample_annot['FORCE_INCLUSION']==True])
            samplesID_to_discard = [s for s in samplesID_to_discard if s not in samples_forced]
        print('>',len(sample_annot_valid),'samples left for analysis (removing duplicates and bad quality samples)')

        return colors_ctypes, patient_cancertype, expnorm_log_renamed[[s for s in expnorm_log_renamed.columns.tolist() if s not in samplesID_to_discard]], genes_annotations, sample_annot_valid
    else:
        expnorm_log = pd.DataFrame()
        norm_counts_s = norm_counts[valid_cols]
        for col in norm_counts_s.columns.tolist():
            if col == 'GENE':
                expnorm_log[col] = norm_counts_s[col]
            else:
                expnorm_log[col] = norm_counts_s[col].apply(lambda x: apply_log(x))
        expnorm_log_renamed = expnorm_log.rename(columns=sampleid_patient)

        return colors_ctypes, patient_cancertype, expnorm_log_renamed, genes_annotations, sample_annot[sample_annot['SAMPLE_ID'].isin(valid_cols)]

